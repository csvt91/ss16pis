package minimal;

/**
 * Created by User on 27.04.2016.
 */
public class ComputerPlayer {
    PlayBoard board;

    public ComputerPlayer(PlayBoard a) {
        this.board = a;
    }

    private int max(int player, int depth) {
        if (depth == 0 || turnsLeft(player))
            return board.threeInARow_01();
        int max = -Integer.MAX_VALUE;
        // generiereMöglicheZüge(spieler)
        while (turnsLeft()) {
            checkMove();
            int tmp = min(-player, depth - 1);
            undoMove();
            if (tmp > max) {
                max = tmp;
                if (depth == maxdepth)
                    makeMove(move);
            }
        }
        return max;
    }

    private int min(int player, int depth) {
        if (depth == 0 || turnsLeft(player))
            return board.threeInARow_01();
        int min = Integer.MAX_VALUE;
        // generiereMöglicheZüge(spieler)
        while (turnsLeft()) {
            checkMove();
            int tmp = max(-player, depth - 1);
            undoMove();
            if (tmp < min) {
                min = tmp;
            }
        }
        return min;
    }
}
