package minimal;

import java.util.ArrayList;

/**
 * Created by Caroline-Sophie Vetter, 977447, on 20.04.2016.
 * Variante 3 - Einfaches Array mit Rand.
 * Für Spieler sichtbar: 6-8; 11-13; 16-18.
 * [0]   [1]  [2]  [3]  [4]
 * [5]   [6]  [7]  [8]  [9]
 * [10] [11] [12] [13] [14]
 * [15] [16] [17] [18] [19]
 * [20] [21] [22] [23] [24]
 */

public class PlayBoard {

    private Integer[] board;
    private int currentPlayer;
    private int gamelength;
    ArrayList<Integer[]> gameHistory;


    public PlayBoard(Integer[] intarray) {
        this.board = intarray;
        this.currentPlayer = 1;
        this.gamelength = 0;
        this.gameHistory = new ArrayList<Integer[]>();
    }

    public void makeMove(int move) {
        //TODO Ausgeben des letzten Spielfelds
        threeInARow(move);
        saveMove();
        this.gamelength++;
        this.currentPlayer = -this.currentPlayer;
    }

    public void showBoard() {
        System.out.println("" + this.board[6] + this.board[7] + this.board[8]);
        System.out.println("" + this.board[11] + this.board[12] + this.board[13]);
        System.out.println("" + this.board[16] + this.board[17] + this.board[18]);
    }

    /**
     * Fügt den letzen Spielzug zur Game History hinzu
     */
    private void saveMove() {
        this.gameHistory.add(board);
    }

    /**
     * Übernimmt das vorletze Spielbrett/den letzten Spielzug und löscht den letzten Eintrag der Game History
     */
    public void undoMove() {
        this.board = gameHistory.get(gameHistory.size() - 1);
        gameHistory.remove(gameHistory.size());
    }

    /**
     * Three in A Row - Eine Methode die NACH dem Spielerzug überprüft ob dieser Spieler gewonnen hat.
     * Index darf kein leeres Feld sein = Spielerzug muss getätigt worden sein. Bei leerem Index = Index out of Bounce
     * Spielerzug X = 1
     * Spielerzug O = -1
     * Neutrales Feld = 0;
     *
     * @param index letzter Spielzug
     * @return Gewinner
     */
    private int threeInARow(int index) {
        int count = 0;
        int i = 0;
        int[] walker = {5, 1, 6, 4};
        for (int a = 0; a < walker.length; a++) {
            count = 1;
            i = index + walker[a];
            while (board[i] == this.currentPlayer) {
                count++;
                i = i + walker[a];
            }
            i = index - walker[a];
            while (board[i] == this.currentPlayer) {
                count++;
                i -= walker[a];
            }
            if (count == 3)
                return this.currentPlayer;
        }
        return 0;
    }

    public int threeInARow_01() {
        // 012, 345, 678, 036, 147, 258, 048, 245
        if (Math.abs(this.board[6] + this.board[7] + this.board[8]) == 3) return this.board[6];
        if (Math.abs(this.board[11] + this.board[12] + this.board[13]) == 3) return this.board[11];
        if (Math.abs(this.board[16] + this.board[17] + this.board[18]) == 3) return this.board[16];
        if (Math.abs(this.board[6] + this.board[11] + this.board[16]) == 3) return this.board[6];
        if (Math.abs(this.board[7] + this.board[12] + this.board[17]) == 3) return this.board[7];
        if (Math.abs(this.board[8] + this.board[13] + this.board[18]) == 3) return this.board[8];
        if (Math.abs(this.board[6] + this.board[12] + this.board[18]) == 3) return this.board[6];
        if (Math.abs(this.board[8] + this.board[12] + this.board[13]) == 3) return this.board[8];
        return 0;
    }
}
